import React from 'react';
import './Social.css';
import cardimg from './../../card.jpg';

const social = props => (

    <section className="social">
        <div class="container">
            <div className="col-12">
                <h1>Contact Us</h1>  
            </div>
            <div className="col-12">
            <ul>
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </section>
);

export default social;