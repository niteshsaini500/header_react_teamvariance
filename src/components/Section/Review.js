import React from 'react';
import './Review.css';
import cardimg from './../../card.jpg';

const review = props => (

    <section className="review">
        <div class="container">
            <div className="col-12">
                <h1>Review</h1>  
                <p>We don't have anything but happy trees here. No worries. No cares. Just float and wait for the wind to blow you around. Tree trunks grow however makes them happy. When things happen - enjoy them. They're little gifts. Maybe, just to play a little, we'll put a little tree here.</p>
            </div>

            <div className="col-12">
            <div className="row justify-content-md-center">
    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                    <img src="https://media-exp1.licdn.com/dms/image/C5103AQEYURD-6amAgQ/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=UOTP0Kq_Qy1fsm1OK6dOXqI0KnUF48v-bXIp_V47T3I" alt="TeamVariance" />
                    </div>
                    <h2>Danish Dewani<span>Cheif Technical Officer</span></h2>
                    <p> Of course he's a happy little stone, cause we don't have any other kind. That's crazy. Let that brush dance around there and play.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                    <img src="https://media-exp1.licdn.com/dms/image/C5103AQHUtJMd4_i4NA/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=k4BDx56sdoBQ_-GoRgYX0ZZgYk1nUEpkGdptY7mU8uE" alt="TeamVariance" />
                    </div>
                    <h2>Nagender Gupta <span>Technical Product Manager</span></h2>
                    <p> These things happen automatically. All you have to do is just let them happen. We'll lay all these little funky little things in there. There he comes.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                    <img src="https://media-exp1.licdn.com/dms/image/C5103AQFis3UbWnnrCg/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=obI5gkpjQcG-aOa6R7uFQNLBlBL5_jpUpFOcYPZbFBc" alt="TeamVariance" />
                    </div>
                    <h2>Srijan Ji Modi<span>Product Developer</span></h2>
                    <p> Trees grow in all kinds of ways. They're not all perfectly straight. Not every limb is perfect. Trees get lonely too, so we'll give him a little friend.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
            </div>

        </div>
    </section>
);

export default review;