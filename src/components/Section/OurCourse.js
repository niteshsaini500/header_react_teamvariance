import React from 'react';
import './OurCourse.css';
import cardimg from './../../card.jpg';

const ourcourse = props => (
    <section className="our-course">
    {/* first list */}
      <div className="container">
        <div className="our-course-outer">
            <div className="row justify-content-md-center">
                <div className="our-course-heading">
                    <h1>Our Team</h1>
                </div>
            </div>            
            <div className="row">
                <div className="col-sm-12 our-course-subheading">
                    <h3>TeamVariance - Secret, Driven by Passion Committed by Intent</h3>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 our-course-subheading">
                <p>Almost everything is going to happen for you automatically - you don't have to spend any time working or worrying. That is when you can experience true joy, when you have no fear. You have to make these big decisions. By now you should be quite happy about what's happening here.</p>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 our-course-subheading">
                <p>Professional: Founder & Cheif Executive Officer, Cheif Marketing Officer, Product Manager</p>
                </div>
            </div>

           <div className="row justify-content-md-center">
    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                        <img src="https://cdn-images-1.medium.com/fit/c/100/100/0*IjJyTU0s2OFbi7V2.jpeg" alt="TeamVariance" />
                    </div>
                    <h2>Raghu Choudhary<span>Founder of TeamVariance</span></h2>
                    <p> We don't want to set these clouds on fire. It's a very cold picture, I may have to go get my coat. It’s about to freeze me to death.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                        <img src="https://media-exp1.licdn.com/dms/image/C5103AQGKL2VFaiJN_A/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=PP20d2N6s7zjbqUzhdftlOpn7ztay6XMgLgIZniuHMs" alt="TeamVariance" />
                    </div>
                    <h2>Tejeshwar Nanda <span>Cheif Marketing Officer</span></h2>
                    <p> All kinds of happy little splashes. Let's have a little bit of fun today. Here we're limited by the time we have. Very easy to work these to death.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                        <img src="https://media-exp1.licdn.com/dms/image/C5103AQEvJjU28s98Bg/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=79kJ7wMXXF9Jjg4en_NL0-RDkCEhkt7G9gPGYIz0zbQ" alt="TeamVariance" />
                    </div>
                    <h2>Shefali Srivast<span>Product Manager</span></h2>
                    <p> I'm gonna add just a tiny little amount of Prussian Blue. It's a super day, so why not make a beautiful sky? We'll put a happy little bush here.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>
      </div>
{/* second list */}
      <div className="container">
        <div className="our-course-outer">
                     
            <div className="row">
                <div className="col-sm-12 our-course-subheading">
                    <h3>Technical - Code can't lie, Comments can. </h3>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 our-course-subheading">
                <p>All you need is a dream in your heart, and an almighty knife. Let's get crazy. Without washing the brush, I'm gonna go right into some Van Dyke Brown, some Burnt Umber, and a little bit of Sap Green.</p>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 our-course-subheading">
                <p>Product Developement: Cheif Technical Officer, Technical Product Manager, Product Developer</p>
                </div>
            </div>

           <div className="row justify-content-md-center">
    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                        <img src="https://media-exp1.licdn.com/dms/image/C5103AQEYURD-6amAgQ/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=UOTP0Kq_Qy1fsm1OK6dOXqI0KnUF48v-bXIp_V47T3I" alt="TeamVariance" />
                    </div>
                    <h2>Danish Dewani<span>Cheif Technical Officer</span></h2>
                    <p> Of course he's a happy little stone, cause we don't have any other kind. That's crazy. Let that brush dance around there and play.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                        <img src="https://media-exp1.licdn.com/dms/image/C5103AQHUtJMd4_i4NA/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=k4BDx56sdoBQ_-GoRgYX0ZZgYk1nUEpkGdptY7mU8uE" alt="TeamVariance" />
                    </div>
                    <h2>Nagender Gupta <span>Technical Product Manager</span></h2>
                    <p> These things happen automatically. All you have to do is just let them happen. We'll lay all these little funky little things in there. There he comes.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                        <img src="https://media-exp1.licdn.com/dms/image/C5103AQFis3UbWnnrCg/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=obI5gkpjQcG-aOa6R7uFQNLBlBL5_jpUpFOcYPZbFBc" alt="TeamVariance" />
                    </div>
                    <h2>Srijan Ji Modi<span>Product Developer</span></h2>
                    <p> Trees grow in all kinds of ways. They're not all perfectly straight. Not every limb is perfect. Trees get lonely too, so we'll give him a little friend.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>
      </div>
      {/* third list */}
      <div className="container">
        <div className="our-course-outer">
                     
            <div className="row">
                <div className="col-sm-12 our-course-subheading">
                    <h3>Designing - Creative, Elegant & Attractive, Design with your imagination. </h3>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 our-course-subheading">
                <p>We'll put some happy little leaves here and there. You have freedom here. The only guide is your heart. Just go back and put one little more happy tree in there.</p>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 our-course-subheading">
                <p>Product Desginers: Creative Graphic Desginer, Graphic Desginer/Illustrator, Graphic Desginer</p>
                </div>
            </div>

           <div className="row justify-content-md-center">
    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                        <img src="https://media-exp1.licdn.com/dms/image/C5103AQEJDgLVoxWD5w/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=8PPBE2zebCXaYWuVATOitujoiDBQ4hATPPpP_DF26BM" alt="TeamVariance" />
                    </div>
                    <h2>Saurabh Chakraborty<span>Creative Graphic Desginer</span></h2>
                    <p> Zip. That easy. Let's just drop a little Evergreen right here. Steve wants reflections, so let's give him reflections.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                        <img src="https://media.licdn.com/dms/image/C5103AQFhfCGr6uDPUQ/profile-displayphoto-shrink_800_800/0?e=1546473600&v=beta&t=mzd_qT3KzKNT_3JKcUeGINsYXYnjCyTcnTZJwU0-Aqo" alt="TeamVariance" />
                    </div>
                    <h2>Ashish K Verma <span>Graphic Desginer/Illustrator</span></h2>
                    <p> Imagination is the key to painting. Isn't that fantastic that you can make whole mountains in minutes? You can do anything here. So don't worry about it.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div className="col-sm-4">
        <div className="our-course-inner">
            <div class="card">
                <div class="box">
                    <div className="img">
                        <img src="https://media-exp1.licdn.com/dms/image/C5103AQEOGoB9Vta69g/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=eoDFAC5Bn9S_S_irDjgkVQ5ezY7Gelvo9em9hCjrjho" alt="TeamVariance" />
                    </div>
                    <h2>Diksha Kumari<span>Graphic Desginer</span></h2>
                    <p> You have to make these big decisions. The shadows are just like the highlights, but we're going in the opposite direction. Learn when to stop.</p>
                    <span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>
      </div>
   </section>
   
);

export default ourcourse;