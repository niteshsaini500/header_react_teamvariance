import React from 'react';
import './Howitworks.css';

const navbarSection= props => (
   <section className="how-it-works">
      <div className="container">
        <div className="row justify-content-md-center">
          <div className="howitworks-heading">
              <h1>How It Works</h1>
          </div>
        </div>
        <div className="row justify-content-md-center">            
            <div className="col-3">
              <div className="howitworks-inner">
                <i class="fa fa-handshake-o" data-unicode="f2b5"></i>
                <h3>Team</h3>
                <p>We Create a team before.</p>
              </div>
            </div>
            <div className="col-3">
              <div className="howitworks-inner">
              <i class="fa fa-heart" data-unicode="f004"></i>
                <h3>Passion</h3>
                <p>Do something by passion.</p>
              </div>
            </div>
            <div className="col-3">
              <div className="howitworks-inner">
              <i class="fa fa-check" data-unicode="f004"></i>
                <h3>Committed</h3>
                <p>Pure committed on our delevery.</p>
              </div>
            </div>
            <div className="col-3">
              <div className="howitworks-inner">
              <i class="fa fa-refresh" data-unicode="f004"></i>
                <h3>Intent</h3>
                <p>Do it by Intent.</p>
              </div>
            </div>
        </div>
      </div>
   </section>
);

export default navbarSection;