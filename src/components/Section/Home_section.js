import React from 'react';
import './Home_section.css';
import Background from './background.png';

var sectionStyle = {
    width: "100%",
    height: "400px",
    backgroundImage: `url(${Background})`,
    backgroundSize:'auto'
  };

const homeSection = props => (
    <section style={ sectionStyle }>
    <div className="container">
        <div className="row">
           <div className="col-12">
           <div className="banner-text">
                  <h1>People First, Always "We"</h1>    
                  <p>We focus on the Inbound Method of acquiring customers with the right message at the right time and in the right context.</p>
                </div>
           </div>
          </div>
        </div>
      </section>
);


export default homeSection;