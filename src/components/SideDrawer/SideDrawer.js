import React from 'react';
import './SideDrawer.css';
import logo from './../../logo.png';

const sideDrawer = props => {
    let drawerClasses = 'side-drawer';

    if(props.show) {
        drawerClasses = 'side-drawer open'; 
    }

    return ( 
        <nav className={drawerClasses}>

        <ul>
        <li className="drawer_logo"><a href="/"><img  src={logo} alt="TeamVariance"/></a></li>
            <li><a href="/">Home</a></li>
            <li><a href="/">Introduction</a></li>
            <li><a href="/">How it works</a></li>
            <li><a href="/">Our Courses</a></li>
            <li><a href="/">Reviews</a></li>
            <li><a href="/">Technology Advisors</a></li>
            <li><a href="/">Login</a></li>
        </ul>
    </nav>
    );
    
    };

export default sideDrawer;