import React, { Component } from 'react';
import { Navbar, NavbarBrand, NavbarNav, NavbarToggler, Collapse, NavItem, NavLink, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'mdbreact';
import { BrowserRouter as Router } from 'react-router-dom';
import './Header.css';


class App extends React.Component {
    
    render() {
        return (
            <Router>
                <section className="second-nav">
                    <div className="container">
                        <div className="row">
                            <div className="col-3">
                                <div className="second-nav-inner">
                                    How It Works
                                </div>
                            </div>
                            <div className="col-3">
                                <div className="second-nav-inner">
                                    Our Courses
                                </div>                                
                            </div>
                            <div className="col-3">
                                <div className="second-nav-inner">
                                    Reviews
                                </div>
                            </div>
                            <div className="col-3">
                                <div className="second-nav-inner">
                                    Advisor
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Router>
        );
    }
}


export default App;
  