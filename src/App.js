import React, { Component } from 'react';
import './App.css';
import Toolbar from './components/Toolbar/Toolbar';
import SideDrawer from './components/SideDrawer/SideDrawer';
import Backdrop from './components/Backdrop/Backdrop';
import HomeSection from './components/Section/Home_section';
import Howitworks from './components/Section/Howitworks';
import OurCourse from './components/Section/OurCourse';
import Review from './components/Section/Review';
import Social from './components/Section/Social';
import Footer from './components/Section/Footer';

import Header from './Header';

class App extends Component {
  state = {
    SideDrawerOpen: false
  };

  drawerToggleClickHandler = () =>{
    this.setState((prevState) => {
      return {sideDrawerOpen: !prevState.sideDrawerOpen};
    });
  };

  backdropClickHandler = () => {
      this.setState({sideDrawerOpen: false});
  };

  render() {
    let backdrop;
    if(this.state.sideDrawerOpen){     
      backdrop = <Backdrop  click={this.backdropClickHandler} />
    }
    return (
      <div style={{height:'100%'}}>
          <Toolbar drawerClickHandler={this.drawerToggleClickHandler}/>
          <SideDrawer show={this.state.sideDrawerOpen}/>
          {backdrop}
          <main style={{marginTop:'0px'}}>
          <HomeSection />
          <Header />
          <Howitworks />
          <OurCourse />
          <Review />
          <Social />
          <Footer />
          </main>    
      </div>
    );
  }
}

export default App;
